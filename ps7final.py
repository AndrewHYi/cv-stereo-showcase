# REDACTED: Prospective Employers: This is the truncated, redacted version of the project. Please contact me at andrew.yi50@gmail.com for full project report and code.

import numpy as np
import cv2
import os
import pdb
import time

# REDACTED: Prospective Employers: This is the truncated, redacted version of the project. Please contact me at andrew.yi50@gmail.com for full project report and code.

def gen_ssd_disparity_graph(L, R, win_size=9, drange=50, border_type=cv2.BORDER_REPLICATE, direction='bi', resize_factor=12, options={}):
    pass

def gen_ssd_disparity_graph_vectorized(L, R, win_size=9, drange=50, border_type=cv2.BORDER_REPLICATE, direction='LR', options={}):
    pass

def E_sum(errors_lst, D, K=3, alpha=1., beta=1.):
    pass

def gen_energy_min_ssd_disparity_graph(L, R, alpha=1., beta=0.35, K=15, win_size=9, drange=50, border_type=cv2.BORDER_REPLICATE, resize_factor=12, direction='LR', options={}):
    pass

def gen_loopy_belief_prop_disparity_graph(left_image, right_image, resize_factor=1., win_size=None, direction='LR', drange=(0, 20), options={}):
    pass