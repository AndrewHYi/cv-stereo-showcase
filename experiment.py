# REDACTED: Prospective Employers: This is the truncated, redacted version of the project. Please contact me at andrew.yi50@gmail.com for full project report and code.

import os
import sys
import pdb
import numpy as np
import cv2
import ps7final
import time

INPUT_DIR = 'input_images'
OUTDUR_DIR = 'output'

# REDACTED: Prospective Employers: This is the truncated, redacted version of the project. Please contact me at andrew.yi50@gmail.com for full project report and code.

# Processing functions

if __name__ == "__main__":
    start = time.time()

    part_1a()
    part_1b()
    part_2a()
    part_2b()
    part_3a()
    part_3b()

    end = time.time()
    print("Finished experiments in: {} seconds".format(round(end - start, 2)))
