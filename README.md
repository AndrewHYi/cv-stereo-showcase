# CV Spring 2018 Final Project: Stereo Correspondence
Andrew Yi (andrewyi@gatech.edu)

Computer Vision - CS6476 Spring 201

Georgia Institute of Technology

## Prospective Employers: This is the truncated, redacted version of the project. Please contact me at andrew.yi50@gmail.com for full project report and code.

### [Report Available Here](reports/report.pdf)


## Video Presentation
* Url: https://www.youtube.com/watch?v=8C-krrzMs9U

![](input_images/middlebury/Art_Half/view1.png)
![](input_images/middlebury/Art_Half/disp1.png)

## Project Files
I've tried my best to have the project structure conform closely to the structure of the previous projects in the course. Included in this project you'll find the following important files and directories:

1. `ps7final.py` contains the stereo correspondence algorithm implementations for SSD and Belief Propagation (I've included my "rough draft" versions/attempts as well for reference),
2. `experiment.py` is the main python file to run to generate the report results,
3. `input_images` directory contains the set of Middlebury stereo pairs (and ground truths) used for the experiments,
4. `output` directory where results for the experiments are spit out (I've kept an archived version of my results in `output/output_archived`),
5. `results.txt`, which is just the copy-pasted python print results of the experiment (i.e. disparity error rates for given sets of parameters and stereo pairs; I just tossed it in there so I could reference it easily in my report) and
6. `report.pdf` the assignment report.


## Requirements and Setup

Library requirements are the same as the ones specified in the assignment - all code was run using python 2.7, OpenCV 2.4.13 and NumPy 1.11+ (the SciPy library was not used).

## Experiment Instructions

Warning: Running *all* the experiments took approximately 80 minutes on my 2015 MacBook Pro (SSD, Loopy BP, for all images, for both LR and RL Disparity maps).

To run the experiment used to generate the output images/results for the assignment, just run `python experiment.py` from the root project directory (same as our past assignments).

Note that there are 6 experiments in total:
1. `part_1a` tests SSD for *Aloe*, *Books* and *Baby1* Middlebury stereo pairs (yields "good" results).
2. `part_1b` tests SSD for *Bowling1* and *Monopoly* Middlebury stereo pairs (yields "bad" results).
3. `part_2a` tests Loopy Belief Propagation for the *Aloe*, *Books* and *Baby1* Middlebury stereo pairs that were used in the previous SSD experiment.
4. `part_2b` tests Loopy Belief Propagation for the *Bowling1* and *Monopoly* Middlebury stereo pairs that were used in the previous SSD experiment.
5. `part_3a` tests Loopy Belief Propagation (using _MaxProduct_ as the smoothness term) for the *Art* Middlebury stereo pair and is benchmarked against "state of the art" algorithms on the Middlebury site.
6. `part_3b` tests Loopy Belief Propagation (using _SumProduct_ as the smoothness term) for the same stereo pair in `part_3a`.
